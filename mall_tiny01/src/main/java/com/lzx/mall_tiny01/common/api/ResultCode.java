package com.lzx.mall_tiny01.common.api;

import lombok.Getter;
import org.mybatis.spring.annotation.MapperScan;

/**
 * @Author: lzx
 * @Date: 2021/4/25 16:55
 */
@Getter
public enum ResultCode {
    SUCCESS(1001,"请求成功"),
    FAILURE(1000,"请求失败"),
    FORBIDDEN(1002,"请求被拦截"),
    UNAUTHORIZED(1003,"没有权限");

    private long code;
    private String msg;

    ResultCode(long code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
