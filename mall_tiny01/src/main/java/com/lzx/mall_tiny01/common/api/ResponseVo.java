package com.lzx.mall_tiny01.common.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author: lzx
 * @Date: 2021/4/25 16:54
 */
@Getter
@ToString
public class ResponseVo<T> {
    private long code;
    private String msg;
    private T data;

    private ResponseVo(ResultCode result, T data) {
        this.code = result.getCode();
        this.msg = result.getMsg();
        this.data = data;
    }
    private ResponseVo(Long code, String msg,T data){
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static <E> ResponseVo<E> success(E data){
        return new ResponseVo<>(ResultCode.SUCCESS,data);
    }

    public static <E> ResponseVo<E> success(E data,String msg){
        return new ResponseVo<>(ResultCode.SUCCESS.getCode(),msg,data);
    }

    public static <E> ResponseVo<E> failure(String msg){
        return new ResponseVo<>(ResultCode.FAILURE.getCode(),msg,null);
    }
    public static <E> ResponseVo<E> forbidden(E data){
        return new ResponseVo<>(ResultCode.FORBIDDEN,data);
    }
    public static <E> ResponseVo<E> unAuthorize(E data){
        return new ResponseVo<>(ResultCode.UNAUTHORIZED,data);
    }
}
