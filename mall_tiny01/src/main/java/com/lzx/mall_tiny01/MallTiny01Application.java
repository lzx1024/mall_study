package com.lzx.mall_tiny01;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@MapperScan({"com.lzx.mall_tiny01.mbg.mapper","com.lzx.mall_tiny01.dao"})
public class MallTiny01Application {
    public static void main(String[] args) {
        SpringApplication.run(MallTiny01Application.class, args);
    }
}
