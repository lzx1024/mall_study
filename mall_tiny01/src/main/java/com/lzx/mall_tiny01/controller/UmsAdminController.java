package com.lzx.mall_tiny01.controller;

import cn.hutool.core.bean.BeanUtil;
import com.lzx.mall_tiny01.common.api.ResponseVo;
import com.lzx.mall_tiny01.dto.UmsAdminLoginDto;
import com.lzx.mall_tiny01.mbg.model.UmsAdmin;
import com.lzx.mall_tiny01.mbg.model.UmsPermission;
import com.lzx.mall_tiny01.service.UmsAdminService;
import io.jsonwebtoken.lang.Strings;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

/**
 * @Author: lzx
 * @Date: 2021/4/29 9:20
 */
@Api(tags = "UmsAdminController", description = "后台用户管理")
@RestController
@RequestMapping("/admin")
public class UmsAdminController {
    @Value("${jwt.tokenHead}")
    private String tokenHead;
    @Autowired
    private UmsAdminService adminService;
    @PostMapping("register")
    @ApiOperation(value = "用户注册")
    public ResponseVo register(@RequestBody UmsAdmin adminParam){
        UmsAdmin register = adminService.register(adminParam);
        if(BeanUtil.isEmpty(register)){
            return ResponseVo.failure("注册失败");
        }
        return ResponseVo.success(register,"注册成功");
    }

    @PostMapping("login")
    @ApiOperation(value = "用户登录")
    public ResponseVo login(@Valid @RequestBody UmsAdminLoginDto adminloginDto){
        String token = adminService.login(adminloginDto.getUsername(), adminloginDto.getPassword());
        if(BeanUtil.isEmpty(token)){
            return ResponseVo.failure("用户名或密码错误");
        }
        HashMap<String, String> tokenMap = new HashMap<>(2);
        tokenMap.put("token",token);
        tokenMap.put("tokenHead",tokenHead);
        return ResponseVo.success(tokenMap,"登录成功");
    }
    @ApiOperation("获取用户所有权限")
    @GetMapping("/permission")
    public ResponseVo getPermissionList(@RequestParam Long adminId){
        List<UmsPermission> permissionList = adminService.getPermissionList(adminId);
        return ResponseVo.success(permissionList);
    }
}
