package com.lzx.mall_tiny01.controller;

import com.lzx.mall_tiny01.common.api.ResponseVo;
import com.lzx.mall_tiny01.service.UmsMemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: lzx
 * @Date: 2021/4/26 14:54
 */
@RestController
@Api(tags = "UmsMemberController",description = "会员登录注册管理")
@RequestMapping("sso")
public class UmsMemberController {
    @Autowired
    private UmsMemberService memberService;

    @GetMapping("getAuthCode")
    @ApiOperation("获取验证码")
    public ResponseVo getAuthCode(@RequestParam String telephone){
        return memberService.generateAuthCode(telephone);
    }
    @ApiOperation("判断验证码是否正确")
    @PostMapping("verifyAuthCode")
    public ResponseVo updatePassWord(@RequestParam String telephone,@RequestParam String authCode){
        return memberService.verifyAuthCode(telephone,authCode);
    }

}
