package com.lzx.mall_tiny01.service;

import com.lzx.mall_tiny01.mbg.model.PmsBrand;

import java.util.List;

/**
 * @Author: lzx
 * @Date: 2021/4/26 11:03
 */

public interface PmsBrandService {
    List<PmsBrand> listAllBrand();

    int createBrand(PmsBrand brand);

    int updateBrand(Long id, PmsBrand brand);

    int deleteBrand(Long id);

    List<PmsBrand> listBrandByLimit(int pageNum, int pageSize);

    PmsBrand getBrand(Long id);

}
