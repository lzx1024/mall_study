package com.lzx.mall_tiny01.service;

import com.lzx.mall_tiny01.mbg.model.UmsAdmin;
import com.lzx.mall_tiny01.mbg.model.UmsPermission;

import java.util.List;

/**
 * @Author: lzx
 * @Date: 2021/4/27 10:54
 */
public interface UmsAdminService {

    UmsAdmin getAdminByUsername(String username);

    List<UmsPermission> getPermissionList(Long id);

    UmsAdmin register(UmsAdmin admin);

    String login(String username,String password);
}
