package com.lzx.mall_tiny01.service;

import com.lzx.mall_tiny01.common.api.ResponseVo;

/**
 * @Author: lzx
 * @Date: 2021/4/26 14:51
 */
public interface UmsMemberService {
    /**
     * 生成验证码
     * @param telephone
     * @return
     */
    ResponseVo generateAuthCode(String telephone);

    /**
     * 判断验证码是否正确
     * @param telephone
     * @param authCode
     * @return
     */
    ResponseVo verifyAuthCode(String telephone,String authCode);
}
