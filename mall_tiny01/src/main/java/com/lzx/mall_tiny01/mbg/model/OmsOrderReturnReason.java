package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OmsOrderReturnReason {
    private Long id;

    @ApiModelProperty(value="退货类型")
    private String name;

    private Integer sort;

    @ApiModelProperty(value="状态：0->不启用；1->启用")
    private Integer status;

    @ApiModelProperty(value="添加时间")
    private Date createTime;
}