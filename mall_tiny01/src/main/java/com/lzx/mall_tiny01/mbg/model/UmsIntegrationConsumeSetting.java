package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UmsIntegrationConsumeSetting {
    private Long id;

    @ApiModelProperty(value="每一元需要抵扣的积分数量")
    private Integer deductionPerAmount;

    @ApiModelProperty(value="每笔订单最高抵用百分比")
    private Integer maxPercentPerOrder;

    @ApiModelProperty(value="每次使用积分最小单位100")
    private Integer useUnit;

    @ApiModelProperty(value="是否可以和优惠券同用；0->不可以；1->可以")
    private Integer couponStatus;
}