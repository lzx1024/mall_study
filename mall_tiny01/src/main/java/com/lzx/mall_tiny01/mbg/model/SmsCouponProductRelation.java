package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SmsCouponProductRelation {
    private Long id;

    private Long couponId;

    private Long productId;

    @ApiModelProperty(value="商品名称")
    private String productName;

    @ApiModelProperty(value="商品编码")
    private String productSn;
}