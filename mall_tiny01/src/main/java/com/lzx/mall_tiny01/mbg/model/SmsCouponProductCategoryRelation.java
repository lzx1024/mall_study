package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SmsCouponProductCategoryRelation {
    private Long id;

    private Long couponId;

    private Long productCategoryId;

    @ApiModelProperty(value="产品分类名称")
    private String productCategoryName;

    @ApiModelProperty(value="父分类名称")
    private String parentCategoryName;
}