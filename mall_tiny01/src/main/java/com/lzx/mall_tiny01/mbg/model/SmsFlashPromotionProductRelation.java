package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SmsFlashPromotionProductRelation {
    @ApiModelProperty(value="编号")
    private Long id;

    private Long flashPromotionId;

    @ApiModelProperty(value="编号")
    private Long flashPromotionSessionId;

    private Long productId;

    @ApiModelProperty(value="限时购价格")
    private BigDecimal flashPromotionPrice;

    @ApiModelProperty(value="限时购数量")
    private Integer flashPromotionCount;

    @ApiModelProperty(value="每人限购数量")
    private Integer flashPromotionLimit;

    @ApiModelProperty(value="排序")
    private Integer sort;
}