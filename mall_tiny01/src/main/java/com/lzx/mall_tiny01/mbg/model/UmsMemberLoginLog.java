package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UmsMemberLoginLog {
    private Long id;

    private Long memberId;

    private Date createTime;

    private String ip;

    private String city;

    @ApiModelProperty(value="登录类型：0->PC；1->android;2->ios;3->小程序")
    private Integer loginType;

    private String province;
}