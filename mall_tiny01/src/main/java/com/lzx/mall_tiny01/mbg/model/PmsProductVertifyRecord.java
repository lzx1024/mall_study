package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PmsProductVertifyRecord {
    private Long id;

    private Long productId;

    private Date createTime;

    @ApiModelProperty(value="审核人")
    private String vertifyMan;

    private Integer status;

    @ApiModelProperty(value="反馈详情")
    private String detail;
}