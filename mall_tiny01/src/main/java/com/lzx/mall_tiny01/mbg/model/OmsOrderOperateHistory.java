package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OmsOrderOperateHistory {
    private Long id;

    @ApiModelProperty(value="订单id")
    private Long orderId;

    @ApiModelProperty(value="操作人：用户；系统；后台管理员")
    private String operateMan;

    @ApiModelProperty(value="操作时间")
    private Date createTime;

    @ApiModelProperty(value="订单状态：0->待付款；1->待发货；2->已发货；3->已完成；4->已关闭；5->无效订单")
    private Integer orderStatus;

    @ApiModelProperty(value="备注")
    private String note;
}