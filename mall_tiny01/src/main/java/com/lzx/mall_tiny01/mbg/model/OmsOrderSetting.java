package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OmsOrderSetting {
    private Long id;

    @ApiModelProperty(value="秒杀订单超时关闭时间(分)")
    private Integer flashOrderOvertime;

    @ApiModelProperty(value="正常订单超时时间(分)")
    private Integer normalOrderOvertime;

    @ApiModelProperty(value="发货后自动确认收货时间（天）")
    private Integer confirmOvertime;

    @ApiModelProperty(value="自动完成交易时间，不能申请售后（天）")
    private Integer finishOvertime;

    @ApiModelProperty(value="订单完成后自动好评时间（天）")
    private Integer commentOvertime;
}