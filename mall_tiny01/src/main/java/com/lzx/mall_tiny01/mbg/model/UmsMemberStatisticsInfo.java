package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UmsMemberStatisticsInfo {
    private Long id;

    private Long memberId;

    @ApiModelProperty(value="累计消费金额")
    private BigDecimal consumeAmount;

    @ApiModelProperty(value="订单数量")
    private Integer orderCount;

    @ApiModelProperty(value="优惠券数量")
    private Integer couponCount;

    @ApiModelProperty(value="评价数")
    private Integer commentCount;

    @ApiModelProperty(value="退货数量")
    private Integer returnOrderCount;

    @ApiModelProperty(value="登录次数")
    private Integer loginCount;

    @ApiModelProperty(value="关注数量")
    private Integer attendCount;

    @ApiModelProperty(value="粉丝数量")
    private Integer fansCount;

    private Integer collectProductCount;

    private Integer collectSubjectCount;

    private Integer collectTopicCount;

    private Integer collectCommentCount;

    private Integer inviteFriendCount;

    @ApiModelProperty(value="最后一次下订单时间")
    private Date recentOrderTime;
}