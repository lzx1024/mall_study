package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UmsMemberTag {
    private Long id;

    private String name;

    @ApiModelProperty(value="自动打标签完成订单数量")
    private Integer finishOrderCount;

    @ApiModelProperty(value="自动打标签完成订单金额")
    private BigDecimal finishOrderAmount;
}