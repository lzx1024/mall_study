package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SmsFlashPromotion {
    private Long id;

    private String title;

    @ApiModelProperty(value="开始日期")
    private Date startDate;

    @ApiModelProperty(value="结束日期")
    private Date endDate;

    @ApiModelProperty(value="上下线状态")
    private Integer status;

    @ApiModelProperty(value="秒杀时间段名称")
    private Date createTime;
}