package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UmsRole {
    private Long id;

    @ApiModelProperty(value="名称")
    private String name;

    @ApiModelProperty(value="描述")
    private String description;

    @ApiModelProperty(value="后台用户数量")
    private Integer adminCount;

    @ApiModelProperty(value="创建时间")
    private Date createTime;

    @ApiModelProperty(value="启用状态：0->禁用；1->启用")
    private Integer status;

    private Integer sort;
}