package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PmsProductLadder {
    private Long id;

    private Long productId;

    @ApiModelProperty(value="满足的商品数量")
    private Integer count;

    @ApiModelProperty(value="折扣")
    private BigDecimal discount;

    @ApiModelProperty(value="折后价格")
    private BigDecimal price;
}