package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PmsComment {
    private Long id;

    private Long productId;

    private String memberNickName;

    private String productName;

    @ApiModelProperty(value="评价星数：0->5")
    private Integer star;

    @ApiModelProperty(value="评价的ip")
    private String memberIp;

    private Date createTime;

    private Integer showStatus;

    @ApiModelProperty(value="购买时的商品属性")
    private String productAttribute;

    private Integer collectCouont;

    private Integer readCount;

    @ApiModelProperty(value="上传图片地址，以逗号隔开")
    private String pics;

    @ApiModelProperty(value="评论用户头像")
    private String memberIcon;

    private Integer replayCount;

    private String content;
}