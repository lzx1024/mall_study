package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UmsMemberTask {
    private Long id;

    private String name;

    @ApiModelProperty(value="赠送成长值")
    private Integer growth;

    @ApiModelProperty(value="赠送积分")
    private Integer intergration;

    @ApiModelProperty(value="任务类型：0->新手任务；1->日常任务")
    private Integer type;
}