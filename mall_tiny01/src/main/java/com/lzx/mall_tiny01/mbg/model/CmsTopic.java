package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CmsTopic {
    private Long id;

    private Long categoryId;

    private String name;

    private Date createTime;

    private Date startTime;

    private Date endTime;

    @ApiModelProperty(value="参与人数")
    private Integer attendCount;

    @ApiModelProperty(value="关注人数")
    private Integer attentionCount;

    private Integer readCount;

    @ApiModelProperty(value="奖品名称")
    private String awardName;

    @ApiModelProperty(value="参与方式")
    private String attendType;

    @ApiModelProperty(value="话题内容")
    private String content;
}