package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SmsFlashPromotionSession {
    @ApiModelProperty(value="编号")
    private Long id;

    @ApiModelProperty(value="场次名称")
    private String name;

    @ApiModelProperty(value="每日开始时间")
    private Date startTime;

    @ApiModelProperty(value="每日结束时间")
    private Date endTime;

    @ApiModelProperty(value="启用状态：0->不启用；1->启用")
    private Integer status;

    @ApiModelProperty(value="创建时间")
    private Date createTime;
}