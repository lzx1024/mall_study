package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CmsHelpCategory {
    private Long id;

    private String name;

    @ApiModelProperty(value="分类图标")
    private String icon;

    @ApiModelProperty(value="专题数量")
    private Integer helpCount;

    private Integer showStatus;

    private Integer sort;
}