package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PmsMemberPrice {
    private Long id;

    private Long productId;

    private Long memberLevelId;

    @ApiModelProperty(value="会员价格")
    private BigDecimal memberPrice;

    private String memberLevelName;
}