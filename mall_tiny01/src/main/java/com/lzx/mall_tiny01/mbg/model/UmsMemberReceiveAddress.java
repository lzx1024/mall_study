package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UmsMemberReceiveAddress {
    private Long id;

    private Long memberId;

    @ApiModelProperty(value="收货人名称")
    private String name;

    private String phoneNumber;

    @ApiModelProperty(value="是否为默认")
    private Integer defaultStatus;

    @ApiModelProperty(value="邮政编码")
    private String postCode;

    @ApiModelProperty(value="省份/直辖市")
    private String province;

    @ApiModelProperty(value="城市")
    private String city;

    @ApiModelProperty(value="区")
    private String region;

    @ApiModelProperty(value="详细地址(街道)")
    private String detailAddress;
}