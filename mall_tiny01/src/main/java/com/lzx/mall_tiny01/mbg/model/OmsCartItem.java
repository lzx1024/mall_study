package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OmsCartItem {
    private Long id;

    private Long productId;

    private Long productSkuId;

    private Long memberId;

    @ApiModelProperty(value="购买数量")
    private Integer quantity;

    @ApiModelProperty(value="添加到购物车的价格")
    private BigDecimal price;

    @ApiModelProperty(value="销售属性1")
    private String sp1;

    @ApiModelProperty(value="销售属性2")
    private String sp2;

    @ApiModelProperty(value="销售属性3")
    private String sp3;

    @ApiModelProperty(value="商品主图")
    private String productPic;

    @ApiModelProperty(value="商品名称")
    private String productName;

    @ApiModelProperty(value="商品副标题（卖点）")
    private String productSubTitle;

    @ApiModelProperty(value="商品sku条码")
    private String productSkuCode;

    @ApiModelProperty(value="会员昵称")
    private String memberNickname;

    @ApiModelProperty(value="创建时间")
    private Date createDate;

    @ApiModelProperty(value="修改时间")
    private Date modifyDate;

    @ApiModelProperty(value="是否删除")
    private Integer deleteStatus;

    @ApiModelProperty(value="商品分类")
    private Long productCategoryId;

    private String productBrand;

    private String productSn;

    @ApiModelProperty(value="商品销售属性:[{'key':'颜色','value':'颜色'},{'key':'容量','value':'4G'}]")
    private String productAttr;
}