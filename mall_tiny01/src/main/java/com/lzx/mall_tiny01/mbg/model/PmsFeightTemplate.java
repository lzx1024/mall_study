package com.lzx.mall_tiny01.mbg.model;

import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Created by Mybatis Generator on 2021/04/26
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PmsFeightTemplate {
    private Long id;

    private String name;

    @ApiModelProperty(value="计费类型:0->按重量；1->按件数")
    private Integer chargeType;

    @ApiModelProperty(value="首重kg")
    private BigDecimal firstWeight;

    @ApiModelProperty(value="首费（元）")
    private BigDecimal firstFee;

    private BigDecimal continueWeight;

    private BigDecimal continmeFee;

    @ApiModelProperty(value="目的地（省、市）")
    private String dest;
}