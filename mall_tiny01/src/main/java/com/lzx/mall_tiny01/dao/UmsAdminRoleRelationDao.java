package com.lzx.mall_tiny01.dao;

import com.lzx.mall_tiny01.mbg.model.UmsPermission;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: lzx
 * @Date: 2021/4/28 19:12
 */
public interface UmsAdminRoleRelationDao {
    List<UmsPermission> getPermissionList(@Param("adminId") Long adminId);

}
