package com.lzx.mall_tiny01.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: lzx
 * @Date: 2021/4/26 11:45
 */
@Configuration
@MapperScan("com.lzx.mall_tiny01.mgb.mapper")
public class MybatisConfig {
}
